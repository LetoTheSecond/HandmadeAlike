@echo off
SETLOCAL
:: REM setlocal EnableDelayedExpansion 
:: REM set PROGFILES=%ProgramFiles%
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"

mkdir build
pushd build
:: cl -Zi ..\main.cpp user32.lib gdi32.lib
cl /Zi ..\main.cpp user32.lib gdi32.lib
:: cl /I include /Zi /TP src\*.cpp src\*.c
popd

build\main.exe

ENDLOCAL
