#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
// #pragma comment(lib, "user32.lib")
//#pragma comment(lib, "gdi32.lib")
//#pragma comment(lib, "opengl32.lib")

#define internal static 
#define local_persist static 
#define global_variable static

// TODO(casey): This is a global for now.
global_variable bool Running;

global_variable BITMAPINFO BitmapInfo;
global_variable void *BitmapMemory;
global_variable HBITMAP BitmapHandle;
global_variable HDC BitmapDeviceContext;

internal void
Win32ResizeDIBSection(int Width, int Height){
	// TODO(casey): Bulletproof this.
	// Maybe don't free first, free after, then free first if that fails.

	if(BitmapHandle){
		DeleteObject(BitmapHandle);
	}

	if(!BitmapDeviceContext){
		// TODO(casey): Should we recreate these under certain special circumstances
		BitmapDeviceContext = CreateCompatibleDC(0);
	}
	
	BitmapInfo.bmiHeader.biSize = sizeof(BitmapInfo.bmiHeader);
	BitmapInfo.bmiHeader.biWidth = Width;
	BitmapInfo.bmiHeader.biHeight = Height;
	BitmapInfo.bmiHeader.biPlanes = 1;
	BitmapInfo.bmiHeader.biBitCount = 32;
	BitmapInfo.bmiHeader.biCompression = BI_RGB;

	// TODO(casey): Based on ssylvan's suggestion, maybe we can just
	// allocate this ourselves?
	
	BitmapHandle = CreateDIBSection(
		BitmapDeviceContext, &BitmapInfo,
		DIB_RGB_COLORS, &BitmapMemory, 0, 0);
}

internal void
Win32UpdateWindow(HDC DeviceContext, int X, int Y, int Width, int Height){
	StretchDIBits(DeviceContext,
				X, Y, Width, Height,
				X, Y, Width, Height,
				BitmapMemory, &BitmapInfo, DIB_RGB_COLORS, SRCCOPY);
}

LRESULT CALLBACK WindowCallback(HWND w, UINT m, WPARAM wp, LPARAM lp) {
	LRESULT result = 0;
	switch (m) {
		case WM_PAINT: {
			PAINTSTRUCT Paint;
			HDC DeviceContext = BeginPaint(w, &Paint);
			int X = Paint.rcPaint.left;
			int Y = Paint.rcPaint.top;
			int Width = Paint.rcPaint.right - Paint.rcPaint.left;
			int Height = Paint.rcPaint.bottom - Paint.rcPaint.top;
			Win32UpdateWindow(DeviceContext, X, Y, Width, Height);
			EndPaint(w, &Paint);
		} break;
		case WM_SIZE: {
			RECT ClientRect;
			GetClientRect(w, &ClientRect);
			int Width = ClientRect.right - ClientRect.left;
			int Height = ClientRect.bottom - ClientRect.top;
			Win32ResizeDIBSection(Width, Height);
		} break;
		case WM_CLOSE: {
			PostQuitMessage(0);
		} break;
		case WM_DESTROY:{
			PostQuitMessage(0);
		} break;
		default: {
			result = DefWindowProcA(w, m, wp, lp);
		} break;
	}
	return result;
}

int CALLBACK WinMain(HINSTANCE hI,HINSTANCE hPI,LPSTR lpCL,int nCS){
	// HINSTANCE hI;
	// hInstance				= GetModuleHandleA(0);

	WNDCLASSEX winClass		= {0};
	winClass.cbSize			= sizeof(WNDCLASSEX);
	// winClass.style			= CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
	winClass.lpfnWndProc	= WindowCallback;
	winClass.hInstance		= hI;
	//winClass.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	//winClass.hCursor		= LoadCursor(NULL, IDC_ARROW);
	//winClass.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	//winClass.lpszMenuName	= NULL;
	winClass.lpszClassName	= "MainWindowClass";
	//winClass.hIconSm		= LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassExA(&winClass)) {
		MessageBoxA(nullptr, "Could not register window class", "Error", 0);
		return -1;
	}

	HWND hWin;
	hWin = CreateWindowExA(
		WS_EX_CLIENTEDGE, winClass.lpszClassName, "",
		WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) - 750)/2,
		(GetSystemMetrics(SM_CYSCREEN) - 500)/2,
		750, 500,
		nullptr, nullptr, hI, nullptr);
	
	if (!hWin) {
		MessageBoxA(nullptr, "Could not create window", "Error", 0);
		return -1;
	}

	MSG msg;
	for(;;){
		if (PeekMessageA(&msg, nullptr, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				break;
			} else {
				TranslateMessage(&msg);
				DispatchMessageA(&msg);
			}
		}
	}

	return 0;
}



